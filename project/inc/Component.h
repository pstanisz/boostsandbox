#pragma once

namespace boostsandbox
{

/*!
 * @brief Dummy component
 */
class Component
{
public:
    /*!
     * @brief Dummy method
     * @return Dummy '1' value
     */
    int foo() noexcept;
};

}
