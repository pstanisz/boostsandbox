#include <gmock/gmock.h>

#include <Component.h>

int main(int argc, char **argv)
{
    ::testing::InitGoogleMock(&argc, argv);
    
    return RUN_ALL_TESTS();
}

TEST(ComponentTest, testFoo)
{
    ASSERT_NO_THROW({
        boostsandbox::Component obj;
        EXPECT_EQ(1, obj.foo());
    });
}