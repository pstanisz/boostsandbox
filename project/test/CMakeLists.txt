set(COMPONENTS
    component
)

foreach(COMPONENT ${COMPONENTS})
    add_subdirectory(${COMPONENT})
endforeach(COMPONENT)