#include <Component.h>

namespace boostsandbox
{

int Component::foo() noexcept
{
    return 1;
}

}