#include <iostream>
#include <cstdlib>

#include <Version.h>
#include <Component.h>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    // Supported options
    po::options_description helpMsg("Options");
    helpMsg.add_options()
        ("help", "Shows the help message")
        ("version", "Shows the version of application");

    po::variables_map args_map;
    po::store(po::parse_command_line(argc, argv, helpMsg), args_map);
    po::notify(args_map);    

    if (args_map.count("help"))
    {
        std::cout << helpMsg << std::endl;

        return EXIT_SUCCESS;
    }

    if (args_map.count("version"))
    {
        std::cout << "Boost sandbox " << VersionMajor << "." << VersionMinor << std::endl;
        std::cout << "With: googletest-release-1.8.0" << std::endl;

        return EXIT_SUCCESS;
    }

    boostsandbox::Component obj;
    auto result = obj.foo();
    result++;   // warning silenced

    return EXIT_SUCCESS;
}
